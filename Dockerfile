FROM debian:buster

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive

# We don't specify PKG_CONFIG_PATH/CMAKE_PREFIX_PATH here
# as we want to try cmake configuration without it as well

RUN apt-get update -qq &&                    \
  apt-get install -y --no-install-recommends \
  autoconf                                   \
  automake                                   \
  build-essential                            \
  bzip2                                      \
  ca-certificates                            \
  curl                                       \
  git                                        \
  libc-ares-dev                              \
  libcurl4-openssl-dev                       \
  libre2-dev                                 \
  libssl-dev                                 \
  libtool                                    \
  libzstd-dev                                \
  nasm                                       \
  pkg-config                                 \
  python3-dev                                \
  python3-minimal                            \
  sudo                                       \
  zlib1g-dev &&                              \
  rm -rf /var/lib/apt/lists/*

RUN groupadd -g "$APP_GID" tango                               && \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango && \
    usermod -a -G sudo tango                                   && \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN ./common_setup.sh             && \
    ./install_minimum_versions.sh && \
    rm -rf dependencies
